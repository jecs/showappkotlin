package com.jesuscalderon.showappkotlin.views.activitys

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jesuscalderon.showappkotlin.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        if (savedInstanceState == null) {
            loadFragment(0)
        }

    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
        when (menuItem.itemId) {
            R.id.navigation_home -> {

                loadFragment(0)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                loadFragment(1)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_list -> {
                loadFragment(2)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                loadFragment(3)
                return@OnNavigationItemSelectedListener true
            }


        }
        false
    }

    fun loadFragment(numFragment:Int){
        var fragment : Fragment = homeFragment()

        when(numFragment){
            0 -> {
                fragment = homeFragment()
            }
            1 -> {
                fragment = searchFragment()
            }
            2 -> {
                fragment = listFragment()
            }
            3 -> {
                fragment = profileFragment()
            }
        }

        supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment!!, fragment.javaClass.getSimpleName())
            .commit()
    }


    class homeFragment : Fragment(){
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_home, container, false)
        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
        }
    }
    class searchFragment : Fragment(){
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_search, container, false)
        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
        }
    }
    class listFragment : Fragment(){
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_list, container, false)
        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
        }
    }
    class profileFragment : Fragment(){
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_profile, container, false)
        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
        }
    }

}
