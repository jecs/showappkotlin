package com.jesuscalderon.showappkotlin.views.activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.jesuscalderon.showappkotlin.R

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var etEmail: EditText
    lateinit var etPsw: EditText
    lateinit var btnSignIn: Button
    lateinit var tvSignUp: TextView
    lateinit var tvForgotPsw: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        etEmail = findViewById(R.id.etUsername)
        etPsw = findViewById(R.id.etPassword)
        btnSignIn = findViewById(R.id.btnSignIn)
        tvSignUp = findViewById(R.id.tvSignUp)
        tvForgotPsw = findViewById(R.id.tvForgotPsw)

        btnSignIn.setOnClickListener(this)
        tvSignUp.setOnClickListener(this)
        tvForgotPsw.setOnClickListener(this)
    }

    override fun onClick(v: View) {

        when(v.id){
            R.id.btnSignIn -> {
                val mainIntent = Intent(this, MainActivity::class.java)
                startActivity(mainIntent)
                //finish()
            }
            R.id.tvSignUp -> {
                val signUpIntent = Intent(this,SignUpActivity::class.java)
                startActivity(signUpIntent)
            }
            R.id.tvForgotPsw -> {
                val forgotIntent = Intent(this,ForgotPasswordActivity::class.java)
                startActivity(forgotIntent)
            }
        }
    }
}
