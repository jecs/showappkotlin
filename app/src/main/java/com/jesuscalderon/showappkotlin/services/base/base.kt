package com.jesuscalderon.showappkotlin.services.base

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open abstract class Microservice (var meta: MicroserviceMeta)

class MicroserviceMeta (var id_transaction: String, var status: String, var time_elapsed: String) {
    interface STATUS {
        companion object {
            val OK = "SUCCESS"
            val ERROR = "ERROR"
        }
    }
}

open abstract class MicroserviceResponse (var errorCode: Int = 0, var userMessage: String = "")

class MicroserviceCallback<T>(var callback: MicroserviceCallbackResponse<T>) : Callback<T> {
    override fun onResponse(call: Call<T>, response: Response<T>) =
        if (response.body() != null )
            callback.OnSuccesResponse(response.body()!!)
        else
            callback.onFailureResponse(Throwable("Response from service is null (code 7763)"))
    override fun onFailure(call: Call<T>, t: Throwable)  = callback.onFailureResponse(t)
}
interface MicroserviceCallbackResponse<T> {
    fun OnSuccesResponse(result: T)
    fun onFailureResponse(t: Throwable)
}